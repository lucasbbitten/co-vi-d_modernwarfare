﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaskScript : MonoBehaviour
{
    private float delayTime = 1; //Delay to make sure that the mask that was just shot by the player won't be collected

    private void Update()
    {
        if (delayTime > 0)
        {
            delayTime -= Time.deltaTime;
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.collider != null && other.collider.CompareTag("MaskPosition"))
        {
            MaskPositionScript maskPosition = other.collider.gameObject.GetComponent<MaskPositionScript>();
            if (!maskPosition.mask.activeSelf)
            {
                maskPosition.npcController.hasMask = true;
                maskPosition.npcController.indicator.GetComponent<Indicator>().SetIndicatorType();
                maskPosition.animator.SetTrigger("HitMask");
                maskPosition.mask.SetActive(true);
                LevelManager.Instance.withMaskCount++;
                Destroy(this.gameObject);
            }
        }
    }

    //Player recovering the mask on floor
    private void OnTriggerEnter(Collider other)
    {
        if ( other.CompareTag("Player") && delayTime <= 0)
        {
            Destroy(this.gameObject);
            AudioManager.instance.Play("PickupMask");
            FindObjectOfType<ShootingController>().AddMask(1);
        }    
    }
}
