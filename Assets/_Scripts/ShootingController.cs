﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ShootingController : MonoBehaviour
{
    public float weaponRange = 50f;                     
    [SerializeField] private LayerMask hitLayer;


    [SerializeField] private Transform shootingPoint;
    [SerializeField] private float shootForce;
    [SerializeField] private Vector3 shootOffset = Vector3.up;
    [SerializeField] private Transform shootingTarget; // object used just to make sure that the mask will go to the crosshair position
    [SerializeField] private float shootingRate = 0.5f;
    [SerializeField] private float timeToDestroyMask = 10f;

    [SerializeField] private Camera fpsCam;
    [SerializeField] private Image crosshair;
    [SerializeField] private GameObject particles;
    [SerializeField] private GameObject maskPrefab;
    [SerializeField] private TextMeshProUGUI maskCounter;

    private int maskCount;
    private float shootingDelay;
    private bool canShoot = true;
    private Animator animator;
    private bool gameIsPaused;
    private bool levelIsComplete;
    private bool gameIsOver;

    private void Start()
    {
        animator = GetComponent<Animator>();
        maskCount = GameManager.Instance.currentDifficultyData.maskCount;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Ground"))
        {
            canShoot = false;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Ground"))
        {
            canShoot = true;
        }
    }

    void Update()
    {
        if (shootingDelay >= 0)
        {
            shootingDelay -= Time.deltaTime;
        }

        gameIsPaused = InGameScreens.GameIsPaused;
        levelIsComplete = InGameScreens.LevelIsComplete;
        gameIsOver = InGameScreens.GameIsOver;


        // Create a vector at the center of our camera's viewport
        Vector3 lineOrigin = fpsCam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0.0f));

        // Draw a line in the Scene View  from the point lineOrigin in the direction of fpsCam.transform.forward * weaponRange, using the color green
        Debug.DrawRay(lineOrigin, fpsCam.transform.forward * weaponRange, Color.green);

        if (canShoot && shootingDelay <= 0 && !gameIsPaused && !levelIsComplete && !gameIsOver && maskCount > 0 && Input.GetButtonDown("Fire1"))
        {
            Shoot();
        }
    }

    void Shoot()
    {
        AudioManager.instance.Play("Shooting");
        particles.SetActive(true);
        shootingDelay = shootingRate;
        animator.SetTrigger("Shooting");
        GameObject mask = Instantiate(maskPrefab, shootingPoint.position, shootingPoint.rotation * Quaternion.AngleAxis(180, Vector3.up));
        mask.GetComponent<Rigidbody>().AddForce((shootingTarget.position - shootingPoint.position).normalized* shootForce + shootOffset);
        AddMask(-1);
    }

    public void AddMask(int value)
    {
        maskCount += value;
        maskCounter.text = maskCount.ToString();
    }
}
