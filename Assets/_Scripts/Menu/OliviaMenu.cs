﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OliviaMenu : MonoBehaviour
{

    public static OliviaMenu instance;
    
    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
        
        DontDestroyOnLoad(this.gameObject);
    }

}
