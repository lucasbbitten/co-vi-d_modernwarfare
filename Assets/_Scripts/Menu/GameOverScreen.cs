﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverScreen : MonoBehaviour
{

    
    public void MainMenu()
    {
        Time.timeScale = 1f;
        LevelLoader.Instance.LoadNewLevel("MainMenuScene");

    }

    public void Retry()
    {
        Time.timeScale = 1f;
        LevelLoader.Instance.LoadNewLevel(SceneManager.GetActiveScene().name);
    }
    
    
    public void Exit()
    {
        Application.Quit();
    }
}
