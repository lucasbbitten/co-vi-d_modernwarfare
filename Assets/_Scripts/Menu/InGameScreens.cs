﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class InGameScreens : MonoBehaviour
{
    public static bool GameIsPaused = false;
    public static bool LevelIsComplete = false;
    public static bool GameIsOver = false;

    [SerializeField] private GameObject pauseMenu;
    [SerializeField] private GameObject levelComplete;
    [SerializeField] private GameObject gameOver;
    [SerializeField] private GameObject crossHair;



    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }

        // This is to test level complete screen
        if (Input.GetKeyDown(KeyCode.F8))
        {
            LevelComplete();
        }

        // This is to test game over screen
        if (Input.GetKeyDown(KeyCode.F12))
        {
            GameOver();
        }
    }

    public void Resume()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        Time.timeScale = 1f;
        pauseMenu.SetActive(false);
        GameIsPaused = false;
        crossHair.SetActive(true);
    }
    void Pause()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        Time.timeScale = 0f;
        pauseMenu.SetActive(true);
        GameIsPaused = true;
        crossHair.SetActive(false);

    }

    public void MainMenu()
    {
        Time.timeScale = 1f;
        pauseMenu.SetActive(false);
        GameIsPaused = false;
        SceneManager.LoadScene("MainMenuScene");
    }

    public void Retry()
    {
        Time.timeScale = 1f;
        pauseMenu.SetActive(false);
        GameIsPaused = false;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Continue()
    {
        Time.timeScale = 1f;
        pauseMenu.SetActive(false);
        GameIsPaused = false;
        SceneManager.LoadScene("SubwayScene"); //Should replace with the proper level once available
    }

    public void Exit()
    {
        Application.Quit();
    }



    public void LevelComplete()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        Time.timeScale = 0f;
        levelComplete.SetActive(true);
        LevelIsComplete = true;
        crossHair.SetActive(false);
    }

    void GameOver()
    {
        LevelLoader.Instance.LoadNewLevel("GameOverScene");
    }
}
