﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private GameObject mainMenu;
    [SerializeField] private GameObject difficultySelectionScreen;
    [SerializeField] private Slider slider;
    [SerializeField] private TMP_Text text;
    private Animator animator;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        mainMenu.SetActive(true);
        difficultySelectionScreen.SetActive(false);
        if (!AudioManager.instance.GetCurrentPlaying().Contains("MenuMusic"))
        {
            AudioManager.instance.Stop();
            AudioManager.instance.Play("MenuMusic");
        }
    }

    public void SettingsButton()
    {
        SceneManager.LoadScene("SettingsScene");
    }

    public void StartButton()
    {
        mainMenu.SetActive(false);
        difficultySelectionScreen.SetActive(true);
        OliviaMenu.instance.gameObject.SetActive(false);

    }

    public void DifficultySelected(int difficultyLevel)
    {
        GameManager gameManager = GameManager.Instance;

        gameManager.difficulty = (GameDifficulty) difficultyLevel;
        animator.SetTrigger("Start");
        switch (difficultyLevel)
        {
            case 0:
                gameManager.currentDifficultyData = gameManager.easyData;
                break;
            case 1:
                gameManager.currentDifficultyData = gameManager.normalData;
                break;
            case 2:
                gameManager.currentDifficultyData = gameManager.hardData;
                break;
        }

        Destroy(OliviaMenu.instance.gameObject);
        LevelLoader.Instance.LoadNewLevel("ParkScene");
        
    }

    public void InstructionsButton()
    {
        SceneManager.LoadScene("InstructionsScene");
    }

    public void BackButton()
    {
        mainMenu.SetActive(true);
        difficultySelectionScreen.SetActive(false);
        OliviaMenu.instance.gameObject.SetActive(true);

    }

    public void Exit()
    {
        Application.Quit();
    }
    
    public void OnDifficultyHover(int difficultyLevel)
    {
        text.gameObject.SetActive(true);
        switch (difficultyLevel)
        {
            case 0:
                text.text = "<align=center><u><size=200%>Easy</size></u></align>\n\nIn this difficulty indicators will show:\nInfected people without mask - Red\nPeople infected and with mask - Green\nPeople without mask - Orange ";
                break;
            case 1:
                text.text = "<align=center><u><size=200%>Medium</size></u></align>\n\nIn this difficulty indicators will show:\nPeople without mask - Orange \n";
                break;
            case 2:
                text.text = "<align=center><u><size=200%>Hard</size></u></align>\n\nIn this difficulty there are no indicators ";
                break;
        }
    }


}