﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using TMPro;
public class SettingsMenu : MonoBehaviour
{
    public AudioMixer audioMixer;

    public TMP_Dropdown resolutionDropdown;
    Resolution[] resolutions;

    public TMP_Dropdown qualityDropdown;
    void Start()
    {
        GetResolutions();
        GetQualities();
    }

    private void GetQualities()
    {

        qualityDropdown.ClearOptions();
        List<string> qualityOptions = QualitySettings.names.ToList();

        int currentQualityIndex = 0;

        for (var i = 0; i < qualityOptions.Count; i++)
        {
            
            if (QualitySettings.GetQualityLevel() == i)
            {
                currentQualityIndex = QualitySettings.GetQualityLevel();
            }
        }

        qualityDropdown.AddOptions(qualityOptions);
        qualityDropdown.value = currentQualityIndex;
        qualityDropdown.RefreshShownValue();

    }

    void GetResolutions()
    {
        resolutions = Screen.resolutions;
        resolutionDropdown.ClearOptions();

        List<string> options = new List<string>();

        int currentResolutuionIndex = 0;

        for (int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + " x " + resolutions[i].height;
            options.Add(option);
            if (resolutions[i].width == Screen.width &&
                resolutions[i].height == Screen.height)
            {
                currentResolutuionIndex = i;
                Debug.Log(resolutions[i].width + ", " + resolutions[i].height);
            }
        }

        resolutionDropdown.AddOptions(options);
        resolutionDropdown.value = currentResolutuionIndex;
        resolutionDropdown.RefreshShownValue();
    }

    public void SetVolume (float volume){

        audioMixer.SetFloat("Volume", volume);

    }

    public void SetQuality (int qualityIndex){

        QualitySettings.SetQualityLevel(qualityIndex);
    }


    public void SetFullScreen (bool isFullScreen){
        Screen.fullScreen = isFullScreen;
    }

    public void SetResolution (int resolutionIndex){

        Resolution resolution = resolutions[resolutionIndex];

        Screen.SetResolution(resolution.width, resolution.height,Screen.fullScreen);

    }

}
