﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

//Script to change the materials of npcs to pretend that there are more different npcs
[RequireComponent(typeof(SkinnedMeshRenderer))]
public class RandomNpc : MonoBehaviour
{


    [SerializeField] private SkinnedMeshRenderer body;
    [SerializeField] private List<Color> bodyColors = new List<Color>();

    [SerializeField] private SkinnedMeshRenderer hair;
    [SerializeField] private List<Color> hairColors = new List<Color>();

    [SerializeField] private SkinnedMeshRenderer pants;
    [SerializeField] private List<Color> pantsColors = new List<Color>();

    [SerializeField] private SkinnedMeshRenderer shirt;
    [SerializeField] private List<Color> shirtColors = new List<Color>();

    [SerializeField] private SkinnedMeshRenderer shoes;
    [SerializeField] private List<Color> shoesColors = new List<Color>();

    private MaterialPropertyBlock propBlock;
    private int colorID;

    void SetColor(Renderer renderer, List<Color> colors)
    {
        propBlock = new MaterialPropertyBlock();
        colorID = Shader.PropertyToID("_BaseColor");
        renderer.GetPropertyBlock(propBlock);
        propBlock.SetColor(colorID, colors[Random.Range(0, colors.Count)]);
        renderer.SetPropertyBlock(propBlock);

    }

    private void Awake()
    {
        propBlock = new MaterialPropertyBlock();

        if (body != null && bodyColors.Count > 0)
        {
            SetColor(body.GetComponent<Renderer>(), bodyColors);
        }

        if (hair != null && hairColors.Count > 0)
        {
            SetColor(hair.GetComponent<Renderer>(), hairColors);
        }

        if (pants != null && pantsColors.Count > 0)
        {
            SetColor(pants.GetComponent<Renderer>(), pantsColors);
        }

        if (shirt != null && shirtColors.Count > 0)
        {
            SetColor(shirt.GetComponent<Renderer>(), shirtColors);
        }

        if (shoes != null && shoesColors.Count > 0)
        {
            SetColor(shoes.GetComponent<Renderer>(), shoesColors);
        }
    }
}
