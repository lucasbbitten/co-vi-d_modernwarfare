﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Indicator : MonoBehaviour
{
    private NPCController npcController;

    public void GetNPC(NPCController npc)
    {
        npcController = npc;
    }

    private Renderer _renderer;
    private MaterialPropertyBlock propBlock;
    private int colorID;

    private void Start()
    {
        SetIndicatorType();
    }

    public void SetIndicatorType()
    {
        if (npcController != null)
        {
            if (GameManager.Instance.difficulty == GameDifficulty.easy)
            {
                if (npcController.isInfected && !npcController.hasMask)
                {
                    SetColor(Color.red);
                    gameObject.SetActive(true);

                }
                else if (npcController.isInfected && npcController.hasMask)
                {
                    SetColor(Color.green);
                    gameObject.SetActive(true);
                }
                else if (!npcController.hasMask)
                {
                    SetColor(new Color(.86f,.36f,.0f));
                    gameObject.SetActive(true);

                } 
                else
                {
                    gameObject.SetActive(false);
                }

            }
            else if (GameManager.Instance.difficulty == GameDifficulty.medium)
            {
                if (!npcController.hasMask)
                {
                    SetColor(new Color(.86f, .36f, .0f));
                    gameObject.SetActive(true);

                }
                else
                {
                    gameObject.SetActive(false);
                }

            }
            else
            {
                gameObject.SetActive(false);

            }
        }
    }

    public void SetColor(Color color)
    {
        _renderer = GetComponent<Renderer>();
        propBlock = new MaterialPropertyBlock();
        colorID = Shader.PropertyToID("_BaseColor");
        _renderer.GetPropertyBlock(propBlock);
        propBlock.SetColor(colorID, color);
        _renderer.SetPropertyBlock(propBlock);

    }

}
