﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    public static LevelLoader Instance;
    public Animator levelTransition;
    [SerializeField] float transitionTime;
    private static readonly int Start = Animator.StringToHash("Start");

    // Start is called before the first frame update
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this.gameObject);

    }

    public void LoadNewLevel(string sceneName)
    {
        StartCoroutine(LoadLevel(sceneName));
    }

    private IEnumerator LoadLevel(string sceneName)
    {
        levelTransition.SetTrigger(Start);

        yield return new WaitForSeconds(transitionTime);
        AudioManager.instance.Stop();

        SceneManager.LoadScene(sceneName);
    }

}
