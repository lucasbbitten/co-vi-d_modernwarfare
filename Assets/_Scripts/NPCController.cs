﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AnimationType
{
    Running,
    Walking,
    Sitting,
    Talking,
    Idling,
    Listening,
    SittingGround
}

public class NPCController : MonoBehaviour
{
    public bool hasMask;
    public bool isInfected;
    public Indicator indicator;
    public AnimationType animation = AnimationType.Idling;

    private Animator animator;
    private MaskPositionScript maskPosition;

    public void GetMaskPosition(MaskPositionScript maskPositionScript)
    {
        maskPosition = maskPositionScript;
    }



    public void Infected()
    {
        Infected infected = gameObject.AddComponent<Infected>();
        isInfected = true;
        infected.npcController = this;
        indicator.SetIndicatorType();
        LevelManager.Instance.infectedCount++;
    }

    void Start()
    {
        indicator.GetNPC(this);
        indicator.SetIndicatorType();

        if (isInfected)
        {
            Infected();
        }

        animator = GetComponent<Animator>();

        if (maskPosition != null)
        {
            if (maskPosition.npcController.hasMask)
            {
                maskPosition.mask.SetActive(true);
                LevelManager.Instance.withMaskCount++;
            }
            else
            {
                maskPosition.mask.SetActive(false);

            }
            
            
        }

        animator.SetBool(animation.ToString(), true);

        //If the NPC is sitting, we don't want to move their legs on the mask hitting animation
        animator.SetLayerWeight((animation == AnimationType.Sitting || animation == AnimationType.SittingGround)? 1 : 2, 1);
    }
    
}
