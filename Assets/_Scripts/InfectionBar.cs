﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfectionBar : MonoBehaviour
{
    [SerializeField] private Image content;
    [SerializeField] private float lerpSpeed;
    private void Update()
    {
        HandleBar();
    }

    private void HandleBar()
    {
        content.fillAmount = Mathf.Lerp(content.fillAmount, LevelManager.Instance.infectedPercentage,
            Time.deltaTime * lerpSpeed);

    }
    
}
