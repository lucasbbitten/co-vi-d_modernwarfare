﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IntroScene : MonoBehaviour
{

    public float waitTime = 35f; //Intro video lenght
    
    void Start()
    {
        StartCoroutine(waitForIntro());
            
    }


    private void Update()
    {
        if (Input.anyKeyDown)
        {
            SceneManager.LoadScene("MainMenuScene"); //invokes the Menu Scene

        }
    }


    IEnumerator waitForIntro()
    {
        yield return new WaitForSeconds(waitTime); //counting the time the video is running 

        SceneManager.LoadScene("MainMenuScene"); //invokes the Menu Scene
    }




}
