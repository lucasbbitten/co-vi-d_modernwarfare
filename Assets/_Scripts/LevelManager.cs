﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    private List<NPCController> npcControllers;

    public static LevelManager Instance;
    private int npcCount;
    private float _infectedCount;
    
    public float infectedCount
    {
        get => _infectedCount;
        set
        {
            _infectedCount = value;
            GetInfectedPercentage();
        }
    }
    
    private int _withMaskCount;

    public int withMaskCount
    {
        get => _withMaskCount;
        set
        {
            _withMaskCount = value;
            if (withMaskCount == npcCount)
            {
                FindObjectOfType<InGameScreens>().LevelComplete();
            }
        }
    }

    void LevelComplete()
    {
        Debug.Log("Level Completed");
    }
    

    public float infectedPercentage;
    public bool cutsceneEnded;

    void Awake()
    {

        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }

        npcControllers = FindObjectsOfType<NPCController>().ToList();
        npcCount = npcControllers.Count;
    }

    void GetInfectedPercentage()
    {
        infectedPercentage = infectedCount / npcCount;
        //Debug.Log($"Number of People: {npcCount}\nNumber of infected: {infectedCount}\nPercentage: {infectedPercentage}%");
    }
    
}
