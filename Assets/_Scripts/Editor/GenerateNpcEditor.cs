﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GenerateNpc))]
[CanEditMultipleObjects]
public class GenerateNpcEditor : Editor
{
    public override void OnInspectorGUI () {
        DrawDefaultInspector();

        GenerateNpc generateColliders = (GenerateNpc) target;
        
        if (GUILayout.Button("Generate NPC data"))
        {
            generateColliders.GenerateNpcData();
        }
        
        if (GUILayout.Button("Generate colliders only"))
        {
            generateColliders.AddColliders();
        }
        
        if (GUILayout.Button("Delete Colliders"))
        {
            generateColliders.DeleteColliders();
        }
    }
}
