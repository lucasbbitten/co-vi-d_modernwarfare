﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Object = System.Object;

[ExecuteInEditMode]

public class GenerateNpc : MonoBehaviour
{
    [SerializeField] private GameObject model;
    private List<Transform> modelChilds;
    private List<Transform> childs;
    private NPCController npc;
    [SerializeField] private bool destroyThisAfterGenerateColliders = true;
    public void GenerateNpcData()
    {
        if (model)
        {
            if (!GetComponent<NPCController>())
            {
                npc = gameObject.AddComponent<NPCController>();
            }
            else
            {
                npc = GetComponent<NPCController>();
            }
            gameObject.layer = LayerMask.NameToLayer("Person");
            
            GetComponent<Animator>().runtimeAnimatorController =
                model.GetComponent<Animator>().runtimeAnimatorController; 
            
            AddIndicator();
            AddColliders();
            AddAudioSource();
            AddRigidyBody();
            AddRandomNpc();
            AddMaskPosition();
            
            if (destroyThisAfterGenerateColliders)
            {
                DestroyImmediate(this);
                
            }
        }
        else
        {
            Debug.LogWarning($"No Model Attached to {gameObject.name}. Can't generate NPCData without a model.");
        }
    }

    void AddMaskPosition()
    {
        if (GetComponentsInChildren<Transform>().Where(x => x.name == "MaskPosition").ToList().Count == 0)
        {
            
            var modelMaskPosition = model.GetComponentsInChildren<Transform>().Where(x => x.name == "MaskPosition").ToList()[0];
            var head = GetComponentsInChildren<Transform>().Where(
                x => x.name.Contains("mixamorig") && x.name.Split(':')[1]  == "Head"
                ).ToList()[0];
            
            GameObject maskPosition = new GameObject("MaskPosition");
            maskPosition.transform.SetParent(head);
            maskPosition.transform.localPosition = modelMaskPosition.localPosition;
            maskPosition.transform.localScale = modelMaskPosition.localScale;
            maskPosition.tag = "MaskPosition";

            BoxCollider modelMaskPosCollider = modelMaskPosition.GetComponent<BoxCollider>();
            BoxCollider maskCol = maskPosition.AddComponent<BoxCollider>();

            maskCol.center = modelMaskPosCollider.center;
            maskCol.size = modelMaskPosCollider.size;
            MaskPositionScript script = maskPosition.AddComponent<MaskPositionScript>();
            
            Object maskPrefab = AssetDatabase.LoadAssetAtPath("Assets/Prefabs/MASK.prefab", typeof(GameObject));
            var mask = (GameObject) PrefabUtility.InstantiatePrefab((GameObject) maskPrefab);
            mask.transform.SetParent(maskPosition.transform);
            mask.transform.localPosition = Vector3.zero;
            DestroyImmediate(mask.GetComponent<Rigidbody>());
            DestroyImmediate(mask.GetComponent<BoxCollider>());
            script.mask = mask;
            script.animator = GetComponent<Animator>();
            script.npcController = npc;
            mask.SetActive(false);
        }
        

    }

    public void AddRandomNpc()
    {
        if (!gameObject.GetComponent<RandomNpc>())
        {
            gameObject.AddComponent<RandomNpc>();
        }
        else
        {
            Debug.LogWarning($"{gameObject.name} already have a RandomNpc component");

        }
    }
    public void AddRigidyBody()
    {
        if (!gameObject.GetComponent<Rigidbody>())
        {
            Rigidbody rigidbody = gameObject.AddComponent<Rigidbody>();
            rigidbody.mass = 60;
            rigidbody.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
            rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
            
        }
        else
        {
            Debug.LogWarning($"{gameObject.name} already have a Rigidbody component");

        }
    }

    public void AddAudioSource()
    {
        if (!gameObject.GetComponent<AudioSource>())
        {
            AudioSource source = gameObject.AddComponent<AudioSource>();
            source.rolloffMode = AudioRolloffMode.Logarithmic;
            source.spatialBlend = 1;
            source.minDistance = 1;
            source.maxDistance = 5;
        }
        else
        {
            Debug.LogWarning($"{gameObject.name} already have a AudioSource component");

        }
    }
    
    public void AddIndicator()
    {
        if (!GetComponentInChildren<Indicator>())
        {
            var modelIndicator = model.GetComponentInChildren<Indicator>().transform.parent;
            Object indicatorPrefab = AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Indicator.prefab", typeof(GameObject));
                
            var indicator = (GameObject) PrefabUtility.InstantiatePrefab((GameObject) indicatorPrefab);
            indicator.transform.SetParent(this.transform);
            indicator.transform.SetAsFirstSibling();
            indicator.transform.localPosition = modelIndicator.localPosition;
            indicator.transform.localScale = modelIndicator.localScale;
            npc.indicator = indicator.GetComponentInChildren<Indicator>();
        }
        else
        {
            Debug.LogWarning($"{gameObject.name} already have a Indicator");
            npc.indicator = GetComponentInChildren<Indicator>();

        }
        
    }
    
    public void AddColliders()
    {
        var rootCollider = model.GetComponent<CapsuleCollider>();
        if (rootCollider && !GetComponent<CapsuleCollider>())
        {
            CopyCollider(this.gameObject, rootCollider);
        }
        
        
        modelChilds = model.GetComponentsInChildren<Transform>().Where(x => x.name.Contains("mixamorig")).ToList();
            
        childs = GetComponentsInChildren<Transform>().Where(x => x.name.Contains("mixamorig")).ToList();
        
        
        foreach (Transform modelChild in modelChilds)
        {
            foreach (Transform child in childs)
            {
                if (child.name.Split(':')[1] == modelChild.name.Split(':')[1])
                {
                    var modelCollider = modelChild.GetComponent<Collider>();
                    if (modelCollider)
                    {
                        if (!child.GetComponent<Collider>())
                        {
                            CopyCollider(child.gameObject, modelCollider);
                        }
                    }
                }
            }
        }
    }
    void CopyCollider(GameObject target, Collider colliderToCopy)
    {
        if (colliderToCopy.GetType() == typeof(CapsuleCollider))
        {
            CapsuleCollider collToCopy = (CapsuleCollider) colliderToCopy;
            var newCollider = target.AddComponent<CapsuleCollider>();
            newCollider.center = collToCopy. center;
            newCollider.direction = collToCopy.direction;
            newCollider.height = collToCopy.height;
            newCollider.radius = collToCopy.radius;
            newCollider.isTrigger = collToCopy.isTrigger;
        }

        if (colliderToCopy.GetType() == typeof(SphereCollider))
        {
            SphereCollider collToCopy = (SphereCollider) colliderToCopy;
            var newCollider = target.AddComponent<SphereCollider>();
            newCollider.center = collToCopy. center;
            newCollider.radius = collToCopy.radius;
            newCollider.isTrigger = collToCopy.isTrigger;
        }
    }
    public void DeleteColliders()
    {
        childs = GetComponentsInChildren<Transform>().Where(x => x.name.Contains("mixamorig")).ToList();

        if (GetComponent<Collider>())
        {
            DestroyImmediate(GetComponent<Collider>());
        }
        
        foreach (Transform child in childs)
        {
            if (child.GetComponent<Collider>())
            {
                DestroyImmediate(child.GetComponent<Collider>());
            }
        }
    }
}
