﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class CutsceneManager : MonoBehaviour
{
    private PlayableDirector playableDirector;
    private bool skipped = false;
    
    private void Awake()
    {
        playableDirector = GetComponent<PlayableDirector>();
    }

    private void Update()
    {
        if (playableDirector.state == PlayState.Playing && Input.GetKeyDown(KeyCode.Space) && !skipped)
        {
            skipped = true;
            LevelLoader.Instance.levelTransition.SetTrigger("Start");
            StartCoroutine(SkipToFrame());
        }
    }

    void OnEnable()
    {
        playableDirector.stopped += OnPlayableDirectorStopped;
    }

    void OnPlayableDirectorStopped(PlayableDirector aDirector)
    {
        if (playableDirector == aDirector)
            LevelManager.Instance.cutsceneEnded = true;

    }

    void OnDisable()
    {
        playableDirector.stopped -= OnPlayableDirectorStopped;
    }
    
    
    IEnumerator SkipToFrame()
    {
        yield return new WaitForSeconds(1f);
        playableDirector.time = playableDirector.duration;
        yield return null;
        playableDirector.Stop();
    }
    
}
