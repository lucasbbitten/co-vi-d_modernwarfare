﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Project created to the Practical Game Design Class of Centennial College Fall 2020
/// with professor Arben Tapia by the group Noob Games 
/// Group: Dhruv Bandaria, Diogo Queiroz, Felipe Rodrigues, Kendrick Goetia, Lucas Bittencourt 
/// 
/// </summary>

public enum GameDifficulty
{
    easy,
    medium,
    hard    
}

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public GameDifficulty difficulty = GameDifficulty.easy;
    public DifficultyData easyData;
    public DifficultyData normalData;
    public DifficultyData hardData;
    public DifficultyData currentDifficultyData;

    // Start is called before the first frame update
    void Awake()
    {
        Application.targetFrameRate = 60;

        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this.gameObject);
        
    }


}
